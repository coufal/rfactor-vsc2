;
; Script for talking to a specific rFactor server instance. Updated to work
; with both rFactor and rFactor 2 dedicated servers.
;

If $CmdLine[0] > 1 Then
   $command = $CmdLine[1] ; chat, boot, ...
   $serverName = $CmdLine[2]
Else
   Exit 20 ; illegal number of arguments (specify at least a command and the name of the server to talk to)
EndIf

If WinActivate("[CLASS:#32770]", $serverName) And ControlGetText("", "", "[CLASS:Static;INSTANCE:13]") == $serverName Then
   If "boot" == $command Then
	  If $CmdLine[0] == 3 Then
		 $driverName = $CmdLine[3]
		 ControlCommand("", "", "[CLASS:ListBox;INSTANCE:1]", "SelectString", $driverName)
		 $selectedDriverName = ControlCommand("" , "", "[CLASS:ListBox;INSTANCE:1]", "GetCurrentSelection")
		 $trimmedDriverName = StringRegExpReplace($selectedDriverName, "(.*) \(.*\)", "'$1'") ; strip off " (AI)" or " (53ms)" suffixes
		 If StringCompare($driverName, $trimmedDriverName) Then
			ControlClick("", "", "[CLASS:Button;INSTANCE:8]")
		 Else
			Exit 11 ; driver name not found
		 EndIf
	  Else
		 Exit 21 ; illegal number of arguments
	  EndIf
   ElseIf "chat" == $command Then
	  If $CmdLine[0] == 3 Then
		 $chatMessage = $CmdLine[3]
		 ControlSend("", "", "[CLASS:Edit;INSTANCE:1]", $chatMessage, 1)
		 ControlClick("", "", "[CLASS:Button;INSTANCE:9]")
	  Else
		 Exit 22 ; illegal number of arguments
	  EndIf
   EndIf
Else
   Exit 10 ; server not found
EndIf
