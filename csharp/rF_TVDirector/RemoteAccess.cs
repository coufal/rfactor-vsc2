﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace rF_TVDirector
{
    public class RemoteAccess
    {
        #region VARIABLES
        private readonly string _Key = "";
        private readonly IPAddress _IP = IPAddress.Parse("127.0.0.1");
        #endregion VARIABLES

        #region PROPERTIES
        /// <summary>
        /// The key used to access this program from out side
        /// </summary>
        public string Key
        {
            get { return _Key; }
        }

        /// <summary>
        /// Contains the public ip of this computer
        /// </summary>
        public IPAddress PublicIP
        {
            get { return _IP; }
        }

        /// <summary>
        /// Return the full key incl. public ip and port of this computer
        /// </summary>
        public string FullKey
        {
            get
            {
                return _IP + ":" + Config.WebserverPort + "/" + _Key;
            }
        }
        #endregion PROPERTIES

        #region CONSTRUCTOR / DESTRUCTOR
        public RemoteAccess(bool queryPublicIP) {
            if (queryPublicIP)
            {
                _IP = GetPublicIP();
            }
            _Key = CreateKey(15);
        }
        #endregion CONSTRUCTOR / DESTRUCTOR

        #region THREADING
        #endregion THREADING

        #region PUBLIC METHODS
        /// <summary>
        /// Check if data contains the Key
        /// </summary>
        /// <param name="data">data to check</param>
        /// <returns>true if data contains the key</returns>
        public bool CheckKey(string data)
        {
            return data.Contains(_Key);
        }

        /// <summary>
        /// Check if given IPEndPoint is localhost
        /// </summary>
        /// <param name="ip">IPEndpoint to check</param>
        /// <returns>true if it is localhost</returns>
        public bool IsLocalhost(IPEndPoint ip)
        {
            return IsLocalhost(ip.Address);
        }
        /// <summary>
        /// Check if given IPAddress is localhost
        /// </summary>
        /// <param name="ip">IPAddress to check</param>
        /// <returns>true if it is localhost</returns>
        public bool IsLocalhost(IPAddress ip)
        {
            return IPAddress.IsLoopback(ip);
        }
        #endregion PUBLIC METHODS

        #region PRIVATE METHODS
        /// <summary>
        /// Method to create a random key
        /// </summary>
        /// <param name="keyLength">length of the resulting key</param>
        /// <returns>string</returns>
        private static string CreateKey(int keyLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789"; //!@$?_-";
            char[] chars = new char[keyLength];
            Random rd = new Random();

            for (int i = 0; i < keyLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        /// <summary>
        /// Query dyndns for the public ip of this computer
        /// </summary>
        /// <returns></returns>
        public static IPAddress GetPublicIP()
        {
            string ipString = "";
            IPAddress publicIP = IPAddress.Parse("127.0.0.1");
            using (WebClient client = new WebClient())
            {
                string ip = client.DownloadString("http://checkip.dyndns.org");
                string[] a = ip.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');
                ipString = a3[0].Trim();
            }
            if (!IPAddress.TryParse(ipString, out publicIP))
            {
                return IPAddress.Parse("127.0.0.1");
            }
            return publicIP;
        }

        #endregion PRIVATE METHODS
    }
}
