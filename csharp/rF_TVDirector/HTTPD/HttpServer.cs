﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SimpleJson;

namespace rF_TVDirector.HTTPD
{
    public class HttpServer : HttpServer_Abstract
    {

        private HashSet<string> _AllowedMethods = new HashSet<string>();
        private HashSet<string> _AllowedTypes = new HashSet<string>();
        private string[] _Path;
        private string _Action = "";
        private RemoteAccess _Remote = null;
        private Dictionary<string, string> _Query = new Dictionary<string, string>();

        public HttpServer(IPAddress server_ip, int port, RemoteAccess remote) : base(server_ip, port)
        {
            _Remote = remote;
        }

        public override void handleGETRequest(HttpProcessor p)
        {
            Console.WriteLine("request: {0}", p.http_url);

            /***
             * Example:
             * http://localhost:81/switchposition/2/5
             * http_url: /switchposition/2/5
             * 
             * http://localhost:81/positioninfo
             * http_url: /positioninfo
             * 
             */

            string path = p.http_url;
            // Split http_url into path and query parts
            string query = "";
            if (path.Contains("?")){
                query = path.Substring(path.IndexOf("?") + 1);
                path = path.Substring(0, path.IndexOf("?"));
            }

            // Strip any leading forward slash (/)
            while (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }

            // Split path to receive the different parts
            _Path = path.Split('/');
            
            // The first part of the path is the action (What's to do)
            _Action = _Path[0].ToLowerInvariant().Trim();

            // favicon.ico request will be ignored
            if (_Action == "favicon.ico") return;
            
            // Localhost (127.0.0.1) is allowed without key
            if (!_Remote.IsLocalhost((IPEndPoint)p.remote_address))
            {
                // If not accessed from localhost or RemoteAccess is disabled
                // Check if the given key is used and correct
                if (!Config.RemoteAccess || !_Remote.CheckKey(path))
                {
                    frmMain.frm.SetErrorMessage("Invalid request! Missing or wrong key!");
                    p.writeJson(SimpleJson.SimpleJson.SerializeObject(new GeneralResponse("Invalid request! Missing or wrong key!")));
                    return;
                }
            }

            // Init the messagebox of the status window
            frmMain.frm.SetErrorMessage("OK");

            ///
            /// switchposition = switch the currently shown car by position nr
            /// 
            if (_Action == "switchposition")
            {
                int ownPos = 0;
                int newPos = 0;

                // The second and third element is the own cars position and the position to jump to
                ownPos = arrayParseInt(_Path, 1, 0);
                newPos = arrayParseInt(_Path, 2, 0);

                // Check positions
                if (ownPos <= 0)
                {
                    frmMain.frm.SetErrorMessage("Invalid own position");
                    p.writeJson(SimpleJson.SimpleJson.SerializeObject(new SwitchPositionResponse("Invalid own position")));
                    return;
                }
                if (newPos <= 0)
                {
                    frmMain.frm.SetErrorMessage("Invalid new position");
                    p.writeJson(SimpleJson.SimpleJson.SerializeObject(new SwitchPositionResponse("Invalid new position")));
                    return;
                }

                // Now switch to the given position
                // If everything was OK, the result should be an empty string, otherwise the errormessage
                string result = frmMain.tv.SwitchToPosition(newPos, ownPos);
                frmMain.frm.SetErrorMessage(result);
                // return the result to the browser
                p.writeJson(SimpleJson.SimpleJson.SerializeObject(new SwitchPositionResponse(result)));
                return;
            }


            ///
            /// positioninfo = return the current position nr and the seconds how long it is already shown
            /// 
            if (_Action == "positioninfo")
            {
                p.writeJson(SimpleJson.SimpleJson.SerializeObject(new PositionInfoResponse(frmMain.tv.NewPosition, frmMain.tv.PositionSince)));
                return;
            }

            ///
            /// Something went wrong. 
            /// The url contained an unknown action
            /// 
            frmMain.frm.SetErrorMessage("Unknown query type!");
            p.writeJson(SimpleJson.SimpleJson.SerializeObject(new GeneralResponse("Unknown query type!")));
        }

        public override void handlePOSTRequest(HttpProcessor p, StreamReader inputData)
        {
            p.writeJson(SimpleJson.SimpleJson.SerializeObject(new GeneralResponse("POST isn't supported")));
            return;
        }

        private int parseInt(string val, int defaultValue = 0)
        {
            int newVal = 0;
            if (int.TryParse(val, out newVal))
            {
                return newVal;
            }
            return defaultValue;
        }

        private int arrayParseInt(string[] values, int index, int defaultValue = 0)
        {
            if (values.Length > index)
            {
                return parseInt(values[index], defaultValue);
            }
            return defaultValue;
        }
    }

}
