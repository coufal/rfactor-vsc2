﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rF_TVDirector.HTTPD
{
    class GeneralResponse
    {
        /// <summary>
        /// In case of any error, this holds the message
        /// </summary>
        public string error = "";

        public GeneralResponse(string ErrorMsg)
        {
            error = ErrorMsg;
        }
    }

    class SwitchPositionResponse
    {
        /// <summary>
        /// In case of any error, this holds the message
        /// </summary>
        public string error = "";

        public SwitchPositionResponse(string ErrorMsg)
        {
            error = ErrorMsg;
        }
    }

    class PositionInfoResponse
    {
        /// <summary>
        /// Currently shown position
        /// </summary>
        public int position = 0;
        /// <summary>
        /// Position is shown since x.x seconds
        /// </summary>
        public double since = 0.0;
        /// <summary>
        /// In case of any error, this holds the message
        /// </summary>
        public string error = "";

        public PositionInfoResponse(int Position, double Since)
        {
            position = Position;
            since = Since;
            error = "";
        }

        public PositionInfoResponse(string ErrorMsg)
        {
            error = ErrorMsg;
            position = 0;
            since = 0.0;
        }
    }
}
