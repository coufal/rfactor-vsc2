## rF_TVDirector

This is the camera control software which is developed on C# (.NET 4.0).
It is a small program which includes a tiny REST interface to interact with the scoring page.

#### Usage:
1. Simply run the exe (rF_TVDirector.exe). The upcoming window shows the current state of the program and the errormessages (if any).
2. Open your webbrowser and enter the following URL to switch the camera to the car on position 5 (just an example): http://localhost:81/switchposition/20/5
See below for a detailed explanation of the REST interface. 



### REST-Interface
The used urlto access the REST interface consists of several parts and is based on the http protocol (e.g. http://localhost:81/switchposition/20/5).

The first part is the protocol, server and port, which always is http://localhost:81/ for the rF_TVDirector.
It is followed by the action and additional parameters (the "switchposition/20/5" in the above example) which will be explained below.
There are currently 2 actions supported: switchposition and positioninfo


### Actions
#### switchposition 
example: http://localhost:81/switchposition/20/5
The switchposition action is used to switch between the vehicles and needs 2 parameters as in the given example.
The first parameter (the 20 in the example) tells the rF_TVDirector, that the own vehicle is on position 20.
The second parameter (the 5 in the example) tells the rF_TVDirector the position of the vehicle to switch to.

So the url synthax is as follows: http://localhost:81/switchposition/<own vehicles position>/<vehicles position to switch to>

The rF_TVDirector will return a JSON formatted result of this operation.
In case of an error it will look like this:
{
error: "<Error message>"
}

In case everything was OK, the "error" parameter will be empty


#### positioninfo
example: http://localhost:81/positioninfo
This will query rF_TVDirector and returns a JSON formatted result. 

e.g.
{
position: 4,
since: 126.515625,
error: ""
}

This means, that position 4 is shown since 126.515625 seconds.
In case of an error, the error parameter will contain the error message