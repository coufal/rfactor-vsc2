package net.rfactor.livescoring.client.ranking;

import java.io.Serializable;
import java.util.Date;

import net.rfactor.livescoring.VehicleData;

import com.google.gson.annotations.SerializedName;

public class Car implements Serializable {
	private static final long serialVersionUID = -797461093026033370L;

	@SerializedName("name")
	private String m_name;
	
	@SerializedName("vehicleClass")
	private String m_class;
	
	@SerializedName("sectorOne")
	private double m_sectorOne;
	
	@SerializedName("sectorTwo")
	private double m_sectorTwo;
	
	@SerializedName("lapTime")
	private double m_lapTime;
	
	@SerializedName("lapTimeDate")
	private Date m_lapTimeDate;
	
	@SerializedName("lapTimeSession")
	private String m_lapTimeSession;
	
	@SerializedName("lapsDriven")
	private int m_lapsDriven;
	
	private transient VehicleData m_lastData;
	private transient boolean m_vehicleInHotLap;

	public boolean setFastestLap(double d, double e, double f, String session) {
		if (f > 0.01f && (m_lapTime < 0.01f || f < m_lapTime)) {
//			System.out.println("set fastest lap time of " + f);
			m_sectorOne = d;
			m_sectorTwo = e;
			m_lapTime = f;
			m_lapTimeDate = new Date();
			m_lapTimeSession = session;
			return true;
		}
		return false;
	}

	public double getFastestLapTime() {
		return m_lapTime;
	}

	public String getName() {
		return m_name;
	}
	
	public String getVehicleClass() {
		return m_class;
	}
	
	public Date getLapTimeDate() {
		return m_lapTimeDate;
	}
	
	public String getLapTimeSession() {
		return m_lapTimeSession;
	}

	public void setNameAndClass(String vehicleName, String vehicleClass) {
		m_name = vehicleName;
		m_class = vehicleClass;
	}

	public double getFastestSector1() {
		return m_sectorOne;
	}
	public double getFastestSector2() {
		return m_sectorTwo - m_sectorOne;
	}
	public double getFastestSector3() {
		return m_lapTime - m_sectorTwo;
	}

    public VehicleData getLastData() {
        return m_lastData;
    }
    
    public void setLastData(VehicleData data) {
        m_lastData = data;
    }

    public void addLap() {
        m_lapsDriven++;
    }
    
    public boolean inHotLap() {
        return m_vehicleInHotLap;
    }
    
    public void setInHotlap(boolean value) {
        m_vehicleInHotLap = value;
    }
    
    public int getLapsDriven() {
		return m_lapsDriven;
	}
}
