package net.rfactor.livescoring.client.ranking;

import java.util.Comparator;

public class DriverLapcountComperator implements Comparator<Driver> {
	@Override
	public int compare(Driver d1, Driver d2) {
		int d1_lapcount=0, d2_lapcount=0;
		for (Car c : d1.getCars()) {
			d1_lapcount+=c.getLapsDriven();
		}
		for (Car c : d2.getCars()) {
			d2_lapcount+=c.getLapsDriven();
		}
		return d2_lapcount - d1_lapcount;
	}
}