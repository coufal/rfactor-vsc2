package net.rfactor.livescoring.client.endurance;

public interface Penalty {
	public static double DEFAULT_TIME_TO_RESOLVE_PENALTY = (15 * 60); // 15 minutes
	enum Type { DRIVE_THRU, STOP_AND_GO, N_SECOND_STOP, BLACK_FLAG, LAP_DOWN_PENALTY, LAP_UP_PENALTY, REJOIN_PENALTY }
	public Type getType();
	public String getDescription();
	public String getReason();
	public double getTime();
	public double getTimeout();
	public boolean isResolved();
	public void setResolved(boolean isResolved);
	public void clearPenalty(String reason, double time, String user);
	public String getClearReason();
	public double getClearTime();
	public String getClearUser();
	public String getVehicleName();
	public Object getData();
	public void setData(Object data);
}