package net.rfactor.livescoring.client.endurance;

public interface Track {
	public enum RaceMode {
		LAPS, TIME, LAPS_TIME;
	}
	
	public enum State {
		FORMATING("Formating"), 
		STARTING("Starting"), 
		RACING("Racing"), 
		FINISHING("Finishing"), 
		FINISHED("Finished");
		
		private final String m_name;
		
		State(String name) {
			m_name = name;            
		}
		
		public String getName() {
			return m_name;
		}
	}

	public RaceMode getRaceMode();

	public double getStartTime();

	public void setStartTime(double et);

	public State getState();

	public void setState(State state);

	public boolean isWinner(Vehicle vehicle);

	public int getLaps();

	public String getName();

	public double getLapDistance();

	public double getTime();
}
