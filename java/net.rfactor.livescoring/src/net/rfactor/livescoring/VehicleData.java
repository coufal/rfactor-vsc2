package net.rfactor.livescoring;

public class VehicleData {
    private final double m_xPosition;
    private final double m_zPosition;
    private final int m_place;
    private final double m_lapDistance;
    private final double m_pathLateral;
    private final double m_speed;
    private final String m_vehicleName;
    private final String m_driverName;
    private final String m_vehicleClass;
    private final int m_totalLaps;
    private final double m_bestSector1;
    private final double m_bestSector2;
    private final double m_bestLapTime;
    private final double m_lastSector1;
    private final double m_lastSector2;
    private final double m_lastLapTime;
    private final double m_currentSector1;
    private final double m_currentSector2;
    private final int m_numberOfPitstops;
    private final int m_numberOfPenalties;
    private final boolean m_inPits;
    private final int m_finishStatus;
    private final int m_sector;
    private final double m_timeBehindLeader;
    private final int m_lapsBehindLeader;
    private final double m_timeBehindNext;
    private final int m_lapsBehindNext;

    public VehicleData(double xPosition, double zPosition, int place, double lapDistance, double pathLateral, double speed, String vehicleName, String driverName, String vehicleClass, int totalLaps, double bestSector1, double bestSector2, double bestLapTime, double lastSector1, double lastSector2, double lastLapTime, double currentSector1, double currentSector2, double timeBehindLeader, int lapsBehindLeader, double timeBehindNext, int lapsBehindNext, int numberOfPitstops, int numberOfPenalties, boolean inPits, int sector, int finishStatus) {
        m_bestLapTime = bestLapTime;
        m_bestSector1 = bestSector1;
        m_bestSector2 = bestSector2;
        m_currentSector1 = currentSector1;
        m_currentSector2 = currentSector2;
        m_driverName = driverName.trim();
        m_timeBehindLeader = timeBehindLeader;
        m_lapsBehindLeader = lapsBehindLeader;
        m_timeBehindNext = timeBehindNext;
        m_lapsBehindNext = lapsBehindNext;
        m_inPits = inPits;
        m_lastLapTime = lastLapTime;
        m_lastSector1 = lastSector1;
        m_lastSector2 = lastSector2;
        m_numberOfPenalties = numberOfPenalties;
        m_numberOfPitstops = numberOfPitstops;
        m_place = place;
        m_speed = speed;
        m_totalLaps = totalLaps;
        m_vehicleClass = vehicleClass.trim();
        m_vehicleName = vehicleName.trim();
        m_xPosition = xPosition;
        m_zPosition = zPosition;
        m_lapDistance = lapDistance;
        m_pathLateral = pathLateral;
        m_sector = sector;
        m_finishStatus = finishStatus;
    }

    public double getXPosition() {
        return m_xPosition;
    }

    public double getZPosition() {
        return m_zPosition;
    }

    public int getPlace() {
        return m_place;
    }

    public double getSpeed() {
        return m_speed;
    }

    public String getVehicleName() {
        return m_vehicleName;
    }

    public String getDriverName() {
        return m_driverName.trim();
    }

    public String getVehicleClass() {
        return m_vehicleClass;
    }

    public int getTotalLaps() {
        return m_totalLaps;
    }

    public double getBestSector1() {
        return m_bestSector1;
    }

    public double getBestSector2() {
        return m_bestSector2;
    }

    public double getBestLapTime() {
        return m_bestLapTime;
    }

    public double getLastSector1() {
        return m_lastSector1;
    }

    public double getLastSector2() {
        return m_lastSector2;
    }

    public double getLastLapTime() {
        return m_lastLapTime;
    }

    public double getCurrentSector1() {
        return m_currentSector1;
    }

    public double getCurrentSector2() {
        return m_currentSector2;
    }

    public int getNumberOfPitstops() {
        return m_numberOfPitstops;
    }

    public int getNumberOfPenalties() {
        return m_numberOfPenalties;
    }

    public boolean isInPits() {
        return m_inPits;
    }

    public double getLapDistance() {
        return m_lapDistance;
    }

    public double getPathLateral() {
        return m_pathLateral;
    }

    public int getFinishStatus() {
        return m_finishStatus;
    }

    public int getSector() {
        return m_sector;
    }
    
    public double getTimeBehindLeader() {
        return m_timeBehindLeader;
    }

    public int getLapsBehindLeader() {
        return m_lapsBehindLeader;
    }

    public double getTimeBehindNext() {
        return m_timeBehindNext;
    }

    public int getLapsBehindNext() {
        return m_lapsBehindNext;
    }
}
