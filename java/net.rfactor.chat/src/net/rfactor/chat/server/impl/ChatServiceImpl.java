package net.rfactor.chat.server.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import net.rfactor.chat.server.ChatService;
import net.rfactor.chat.server.KickService;
import net.rfactor.serverlog.ServerLog;
import net.rfactor.util.Util;

import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public class ChatServiceImpl implements ChatService, KickService, Runnable, ManagedService {
    private static final String DEFAULT_SERVERNAME = "XS4ALL Endurance-Racing";
    private volatile BundleContext m_context;
    private File m_script;
    private volatile String m_server = DEFAULT_SERVERNAME;
    private volatile boolean m_pause = false;
    private Thread m_thread;
    private volatile boolean m_isRunning;
    private final PriorityBlockingQueue<Command> m_queue = new PriorityBlockingQueue<Command>();
    private volatile ServerLog m_serverLog;
    
    public void start() {
        InputStream source = getClass().getResourceAsStream("/script.exe");
        m_script = m_context.getDataFile("script.exe");
        m_script.delete();
        FileOutputStream target = null;
        try {
            target = new FileOutputStream(m_script);
            Util.copyAndDigest(source, target);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
        	if (source != null) {
        		try {
					source.close();
				}
        		catch (IOException e) {
					e.printStackTrace();
				}
        	}
        	if (target != null) {
        		try {
					target.close();
				}
        		catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
        m_pause = false;
        m_isRunning = true;
        m_thread = new Thread(this, "Script Thread");
        m_thread.start();
    }
    public void stop() {
        m_isRunning = false;
        m_thread.interrupt();
    }
    
    public void sendMessage(String message) {
    	// If chat system is paused, do not add any messages to the queue
    	if (m_pause) {
    		m_serverLog.log(-1, "ScriptPaused", 
    		    "message", message);
    		return;
    	}
    	synchronized (m_queue) {
    		Command command = new Command(m_script.getAbsolutePath(), "chat", m_server, message);
			if (!m_queue.contains(command)) {
				m_queue.put(command);
			}
		}
    }
    
    @Override
    public void setPause(boolean pause) {
    	m_pause = pause;
    }
    
    @Override
    public boolean getPause() {
    	return m_pause;
    }
    
    public void run() {
        while (m_isRunning) {
            try {
                Command command = m_queue.poll(5, TimeUnit.SECONDS);
                if (command != null) {
	                try {
	                    ProcessBuilder processBuilder = new ProcessBuilder();
	                    processBuilder.directory(m_script.getParentFile()).command(command.getArgs());
	                    processBuilder.redirectErrorStream(true);
	                    Process process = processBuilder.start();
	                    net.rfactor.chat.server.impl.ChatServiceImpl.ProcessReader inputReader = new ProcessReader("Input reader for script process", process.getInputStream());
	                    inputReader.start();
	                    PrintWriter outputWriter = new PrintWriter(new OutputStreamWriter(process.getOutputStream()));
	                    outputWriter.println();
	                    outputWriter.flush();
	            		m_serverLog.log(-1, "ExecuteCommand", 
	            		    "command", command);
	                    int result = process.waitFor();
	                    if (result > 0) {
		            		m_serverLog.log(-1, "ExecuteCommandFailed", 
		            		    "command", command, 
		            		    "result", result);
	                    }
	                    inputReader.stopThread();
	                    outputWriter.close();
	                    TimeUnit.MILLISECONDS.sleep(3000);
	                }
	                catch (Exception e) {
	                    // if for some reason we get an exception, we assume the process
	                    // ended (we might get interrupted though, if someone does that, this
	                    // might not be true)
	                    e.printStackTrace();
	                }
                }
            }
            catch (InterruptedException e) {
                // if we are interrupted, we should probably stop, so fall through here
            }
        }
    }
    
    /**
     * For each process that gets started, two ProcessReader threads are started to relay
     * data from the process' stdout and error stream to our own reader.
     */
    private class ProcessReader extends Thread {
        private final InputStream m_input;
        private volatile boolean m_stopped = false;

        ProcessReader(String name, InputStream input) {
            super(name);
            m_input = input;
        }
        
        @Override
        public synchronized void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(m_input));
            String line = "";
            while (!m_stopped && line != null) {
                try {
                    line = reader.readLine();
                }
                catch (IOException e) {
                }
            }
        }
        
        /**
         * Makes this thread stop operating as soon as possible.
         */
        public void stopThread() {
            m_stopped = true;
        }
    }

    @Override
    public void updated(Dictionary properties) throws ConfigurationException {
        if (properties == null) {
            // no settings or existing settings deleted, revert to defaults
            m_server = DEFAULT_SERVERNAME;
        }
        else {
            // received settings, first validate them and then apply them
            Object servername = properties.get(SERVERNAME_KEY);
            if (servername instanceof String && !((String) servername).isEmpty()) {
                m_server = (String) servername;
//               	System.out.println("Server: " + m_server);
            }
            else {
                throw new ConfigurationException(SERVERNAME_KEY, "Must be a non-empty string.");
            }
        }
    }
	@Override
	public void kick(String driverName) {
    	if (m_pause) {
    		m_serverLog.log(0, "KickPaused", 
    		    "driver", driverName);
    		return;
    	}
    	synchronized (m_queue) {
    		Command command = new Command(m_script.getAbsolutePath(), "boot", m_server, driverName);
			if (!m_queue.contains(command)) {
				m_queue.put(command);
			}
		}
	}
}
