package net.rfactor.chat.server.impl;

import java.util.Properties;

import net.rfactor.chat.server.ChatService;
import net.rfactor.chat.server.KickService;
import net.rfactor.serverlog.ServerLog;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, ChatService.CHATSERVICE_PID);
        manager.add(createComponent()
            .setInterface(new String[] { ChatService.class.getName(), KickService.class.getName(), ManagedService.class.getName() }, props)
            .setImplementation(ChatServiceImpl.class)
            .add(createServiceDependency().setService(ServerLog.class).setRequired(false))
        );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
