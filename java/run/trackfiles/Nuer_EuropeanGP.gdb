Nuer_EuropeanGP
{
  Filter Properties = FISI_ROAD,ISIFM,TMOD,F2_2012,GTWEC,WSFR
  Attrition = 30
  TrackName = Nuerburg GP
  EventName = European Grand Prix
  VenueName = Nuerburg
  VenueIcon = Nuerburg\NuerburgIcon.dds
  Location = Nuerburg, Germany
  Length = 5.148 km / 3.199 Miles
  TrackType = Road Course
  Track Record = , 89.468
  HeadlightsRequired = true  // whether headlights are required at night
  TerrainDataFile = ..\Nuerburg.tdf         // terrain file override
  MaxVehicles=33

  GarageDepth = 8.0
  TestDaystart = 12:00
  Practice1Start = 11:00
  Practice2Start = 14:00
  Practice3Start = 11:00
  QualifyStart = 14:00
  RaceStart = 14:00
  RaceLaps = 60

  RearFlapZone               // zones for using rear flaps in race sessions
  {
    MinimumCrossings=3       // how many times you must cross the detection under green before it is allowed
    TimeThreshold=1.0        // seconds behind another vehicle for it to be allowed
    DetectionLapDist=3282    // distance around track where time threshold is detected
    ActivationLapDist=3660   // beginning of zone where it can be used (if under time threshold is detected)
    DeactivationLapDist=4370 // end of zone (yes, the zone can be wrapped around s/f, or not)
  }

  NumStartingLights=5


  ShadowMinSunAngle=20.0
  NightLightThreshold=0.5
  SunApeture=0.06

  ShadowMinDiffusion=-0.6
  ShadowMaxDiffusion=0.2

  Latitude = 50.33            // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  Longitude = 6.94         // degs from GMT (-180 to 180, positive is East)
  Altitude = 320            // meters above sea level
  RaceDate = Jul 20   // default date for the race
  TimezoneRelativeGMT = 1.0  // hours compared to Greenwich Mean Time (should be negative to the West)
  DSTRange=(1.0, 1.0, 0, 9999) // (start day 1.0-367.0, end day 1.0-367.0, start year, end year); multiple lines allowed
// U.S. example, where DST changed in 2007 (previous and future changes not simulated here):
//  DSTRange=(94.8, 301.3, 1987, 2006) // approximation of 1st Sunday in April to last Sunday in October
//  DSTRange=(70.8, 308.3, 2007, 9999) // approximation of 2nd Sunday in March to first Sunday in November



///////////////////////////SCORETOWER DATA////////////////////////////////////////////

ScoreboardFont=NUERBURG_SCOREFONT.tga // default is scoreboardfont.bmp
ScoreboardBackground=SCORETOWERBKG.tga // default is scoreboardbkg.bmp

ScoreboardMaxEntries=10 // how many car numbers can be displayed on tower (default is 32)
ScoreboardStartX=0 // x-position in texture to write first car number (default is 0)
ScoreboardStartY=1 // y-position in texture to write first car number (default is 10)
ScoreboardIncX=0 // increment in x-position for each new car number (default is 0)
ScoreboardIncY=43 // increment in y-position for each new car number (default is 16)
ScoreboardScaleX=2.3 // scale multiplier for x (default is 1.0)
ScoreboardScaleY=1.8 // scale multiplier for y (default is 1.0)

//////////////////////////////////////////////////////////////////////////////////////

  SettingsFolder = European_GP
  SettingsCopy = European_GP.svm
  SettingsAI = European_GP.svm
}
