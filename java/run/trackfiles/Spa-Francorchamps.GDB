SpaLEAGUE
{
//--------------------Track Infos--------------------//
  Filter Properties = TMOD * 
  Attrition = 30
  NumStartingLights=6

  TrackName = Spa-Francorchamps
  EventName = Belgian Grand Prix
  GrandPrixName = Belgian Grand Prix
  VenueName = Circuit de Spa-Francorchamps
  VenueIcon = Spa-Francorchamps\Spa-FrancorchampsIcon.dds
  Location = Spa-Francorchamps, Belgium

  Length = 7.004 km / 4.352 miles
  TrackType = Permanent Road Course
  Track Record = 1:45.108 - K Raikkonen (2004)

  TerrainDataFile = ..\Spa.tdf 


//----------------Date and Hours Infos---------------//
  GarageDepth = 1.9
  FormationSpeedKPH = 250.00
  RacePitKPH = 100.00
  QualPitKPH = 100.00
  NormalPitKPH = 80.00
  
  TestDaystart = 11:00

  Practice1Day = Friday		// Practice 1
  Practice1Start = 10:00
  Practice1Duration = 90
  
  Practice2Day = Friday   // Practice 2
  Practice2Start = 14:00
  Practice2Duration = 90
  
  Practice3Day = Saturday  // Practice 3
  Practice3Start = 11:00
  Practice3Duration = 60

  QualifyDay = Saturday		// Qualy
  QualifyStart = 14:00
  QualifyDuration = 15
  QualifyLaps = 99

  RaceDay = Sunday		// Race
  RaceStart = 14:00
  RaceLaps = 44
  RaceTime = 120

//---------------------------------------------------//
  CONE = ( 2.5, 0.25, 0.20, 0.25, 1425.6, 72.0, 0.80 )		// default ( 2.5, 0.25, 0.20, 0.25, 1425.6, 72.0, 0.80 )	( 10.0, 8.0, 4.0, 12.0, 2048.0, 89.6, 2.80 )
  POST = ( 1.2, 0.5, 0.1, 0.5, 400.0, 40.0, 0.80 )		// default ( 1.2, 0.5, 0.1, 0.5, 400.0, 40.0, 0.80 )
  SIGN = ( 5.0, 4.0, 2.0, 6.0, 1024.0, 44.8, 0.70 )		// default inertia parameters for signs are actually calculated based on geometry ( 5.0, 4.0, 2.0, 6.0, 1024.0, 44.8, 0.70 )
//---------------------------------------------------//



RearFlapWetThreshold=0.5
  RearFlapZone
  {
    MinimumCrossings=4
    TimeThreshold=1.0
    DetectionLapDist=809.6
    ActivationLapDist=1602.4
    DeactivationLapDist=2290.9
  }

	CutLeavingThresh = 2.5      // Threshold for leaving track
	CutJoiningThresh = 1.0      // Threshold for re-joining track
	CutPassPenalty = 0.40       // Penalty for each driver we pass (this is an inaccurate indicator)
	CutTimeRatioMax = 0.95      // Start penalizing even if we lose 5% of time
	CutTimeRatioPenalty = 0.40  // Penalty for fraction of time savings
	CutSecondsSaved = 0.675     // Penalty for each second saved
	CutWorstYawPenalty = 1.5    // Penalty for staying under control
	CutDirectionalPenalty = 1.0 // Penalty (actually reward) for being in wrong direction
	CutWarningThresh = 1.75     // Threshold for warning (or disallowance of lap)
	CutStopGoThresh = 3.0       // Threshold for immediate stop/go penalty


  ShadowMinSunAngle=15.0
  NightLightThreshold=0.5
  SunApeture=0.06

  Latitude = 50.26       // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  Longitude = 5.58         // degs from GMT (-180 to 180, positive is East)
  Altitude = 395.0            // meters above sea level	
  RaceDate = August 28, 2011   // default date for the race
  TimezoneRelativeGMT = 1.0  // hours compared to Greenwich Mean Time (should be negative to the West)


//--------------------Setup Infos--------------------//
  SettingsFolder = Spa-Francorchamps-LEAGUE
  SettingsCopy = Grip.svm
  SettingsCopy = SpaLEAGUE.svm
  SettingsAI = SpaAI.svm

}
