package net.rfactor.sync.index;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import net.rfactor.sync.Index;
import net.rfactor.sync.util.IOUtil;
import net.rfactor.sync.util.Indexer;

import org.osgi.service.log.LogService;

public class SyncIndex implements Index {
    private LogService m_log;
	private List<Indexer> m_indexers;

	public void start() {
		// Load configuration, scan index.
		rescan();
	}
	
	public void rescan() {
		// TODO we might want to off-load this to a background thread
		try {
		    m_log.log(LogService.LOG_INFO, "Loading index configuration...");
			Properties p = new Properties();
			m_indexers = new ArrayList<Indexer>();
			File indexProperties = new File("index.properties");
			if (!indexProperties.isFile()) {
			    m_log.log(LogService.LOG_INFO, "Could not locate index configuration: " + indexProperties.getAbsolutePath());
				return;
			}
			p.load(new FileInputStream(indexProperties));
			String pathsValue = p.getProperty("paths");
			String dataValue = p.getProperty("data");
			if (dataValue == null) {
                m_log.log(LogService.LOG_ERROR, "No 'data' setting in configuration file.");
                return;
			}
			File data = new File(dataValue);
            if (!data.isDirectory()) {
                m_log.log(LogService.LOG_ERROR, "Supplied 'data' setting is not an existing directory.");
                return;
            }
			if (pathsValue != null) {
				String[] paths = pathsValue.split(",");
				for (int i = 0; i < paths.length; i++) {
					String path = paths[i];
					String name = path.trim();
					File file = new File(name);
					if (file.isDirectory()) {
					    m_log.log(LogService.LOG_INFO, "Scanning index path " + name + ". Please hold on.");
						File config = new File(data, IOUtil.calculateSHA(new ByteArrayInputStream(name.getBytes())) + ".index");
						Indexer indexer = Indexer.createSyncIndex(new File(name), config);
						indexer.save(new FileOutputStream(config));
						m_indexers.add(indexer);
					}
					else {
					    m_log.log(LogService.LOG_INFO, "Index path " + name + " does not exist. Ignoring it.");
					}
				}
				m_log.log(LogService.LOG_INFO, "Done (re)loading indices.");
			}
			else {
                m_log.log(LogService.LOG_ERROR, "No 'paths' setting in configuration file.");
                return;
			}
		}
		catch (Exception e) {
		    m_log.log(LogService.LOG_ERROR, "Exception while (re)loading indices.", e);
		}
	}
 	
	public void stop() {
		// Unload, flush index to disk?
	}

	@Override
	public File lookup(String hash) {
		for (Indexer indexer : m_indexers) {
			File result = indexer.lookup(hash);
			if (result != null) {
				return result;
			}
		}
		return null;
	}
	
	@Override
	public Set<String> hashes() {
		Set<String> result = new HashSet<String>();
		for (Indexer indexer : m_indexers) {
			result.addAll(indexer.hashes());
		}
		return result;
	}
}
