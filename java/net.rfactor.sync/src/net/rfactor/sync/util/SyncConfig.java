package net.rfactor.sync.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.rfactor.sync.Index;
import net.rfactor.sync.util.Indexer.Config;
import net.rfactor.sync.util.Indexer.FileData;

/**
 * Swiss army knife of synchronization code. Used for server and client.
 */
public class SyncConfig {
	private Indexer m_indexer;
	private List<Pattern> ignores = new ArrayList<Pattern>();
	private List<HashedFile> files = new ArrayList<HashedFile>();
	private Map<String, String> names = new HashMap<String, String>();
	private Map<String, List<String>> shas = new HashMap<String, List<String>>();

	public void write(OutputStream os) throws IOException {
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
		for (Pattern i : ignores) {
			bw.write(i.pattern() + " : ignore\n");
		}
		for (HashedFile sf : files) {
			bw.write(sf.getFile() + " : " + sf.getSha() + "\n");
		}
		bw.close();
	}
	
	public void read(InputStream is) throws IOException {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
	
			String line = br.readLine();
			Pattern pattern = Pattern.compile("(.*) : (ignore|.{40})");
			while (line != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches()) {
					String g1 = matcher.group(1);
					String g2 = matcher.group(2);
					if ("ignore".equals(g2)) {
						addIgnore(Pattern.compile(g1));
					}
					else {
						add(new HashedFile(g1, g2));
					}
					System.out.println("Found: " + g1 + " -> " + g2);
				}
				line = br.readLine();
			}
		}
		finally {
			is.close();
		}
	}

	public String lookup(String file) {
		return names.get(file);
	}
	public List<HashedFile> getFiles() {
		return files;
	}
	public int getNumberOfFiles() {
		return files.size();
	}
	public int getDuplicates() {
		int duplicates = 0;
		for (Entry<String, List<String>> s : shas.entrySet()) {
			int size = s.getValue().size();
			if (size > 1) {
				duplicates += size - 1;
			}
		}
		return duplicates;
	}
	public void setIndexer(Indexer indexer) {
		m_indexer = indexer;
	}
	public Indexer getIndexer() {
		return m_indexer;
	}
	public void setIgnores(ArrayList<Pattern> arrayList) {
		ignores.clear();
		ignores.addAll(arrayList);
	}
	public void addIgnore(Pattern entry) {
		ignores.add(entry);
	}
	public void add(HashedFile sf) {
		files.add(sf);
		names.put(sf.getFile(), sf.getSha());
		List<String> files = shas.get(sf.getSha());
		if (files == null) {
			files = new ArrayList<String>();
		}
		files.add(sf.getFile());
		shas.put(sf.getSha(), files);
	}
	public boolean ignore(String file) {
		for (Pattern p : ignores) {
			if (p.matcher(file).matches()) {
				return true;
			}
		}
		return false;
	}

	public static SyncConfig scan(File baseDir, SyncSourceConfig ssc, File config) throws Exception {
		Indexer si = new Indexer(baseDir, config);
		SyncConfig sc = new SyncConfig();
		sc.setIndexer(si);
		Indexer.Config oldConfig = si.getConfig();
		si.reset();
		sc.scan(baseDir, baseDir, ssc, oldConfig);
		sc.setIgnores(new ArrayList<Pattern>(ssc.getIgnores()));
		return sc;
	}
	
	private void scan(File basedir, File directory, SyncSourceConfig ssc, Indexer.Config oldConfig) throws Exception {
		System.out.println("Scanning folder: " + directory);
		File[] files = directory.listFiles();
		int index = basedir.getPath().length() + 1;
		for (File f : files) {
			if (f.isFile()) {
				String file = f.getPath().substring(index).replace('\\', '/');
				boolean include = ssc.include(file);
				if (include) {
					FileData data = oldConfig.get(file);
					if (data == null || !data.equals(f)) {
						data = new FileData(f);
						System.out.println("File has changed: " + file);
					}
					m_indexer.getConfig().put(file, data);
					HashedFile sf = new HashedFile(file, data.getHash());
					add(sf);
				}
			}
		}
		for (File f : files) {
			if (f.isDirectory()) {
				scan(basedir, f, ssc, oldConfig);
			}
		}
	}
	
	public SyncOps check(File basedir, File trashdir, Index index, String download, File config, Progress progress) throws Exception {
		System.out.println("Checking local folder, comparing with server...");
		
		Indexer si = new Indexer(basedir, config);
		setIndexer(si);
		Indexer.Config oldConfig = si.getConfig();
		si.reset();
		
		SyncOps so = new SyncOps();
		Indexer trashIndex = new Indexer(trashdir, new File(trashdir, "trash.index"));
		so.setDownloads(getFiles());
		check(basedir, basedir, so, oldConfig);
		boolean simulate = false;
		for (HashedFile file : so.getDeleteFiles()) {
			File from = new File(basedir, file.getFile());
			File to = new File(trashdir, file.getSha());
			System.out.println("Moving to trash: " + from + " -> " + to);
			if (!simulate) {
				try {
					if (!to.exists()) {
						IOUtil.move(from, to);
					}
					else {
						IOUtil.delete(from);
					}
				}
				catch (IOException e) {
					System.out.println("Move failed!");
					e.printStackTrace();
				}
			}
			trashIndex.addFile(file.getSha(), to);
		}
		List<HashedFile> downloads = so.getDownloadFiles();
		System.out.println("Assembling folder from " + downloads.size() + " files. Collecting local files.");
		// first pass, fetch all local files
		for (int i = 0; i < downloads.size(); i++) {
			HashedFile file = downloads.get(i);
			if (file != null) {
				File dest = new File(basedir, file.getFile());
				File parent = dest.getParentFile();
				if (!parent.isDirectory()) {
					parent.mkdirs();
				}
				File found = trashIndex.lookup(file.getSha());
				if (found != null) {
					System.out.println("Restoring from trash: " + file);
					if (!simulate) {
						try {
							IOUtil.copy(new FileInputStream(found), new FileOutputStream(dest));
						}
						catch (IOException e) {
							System.out.println("Restore from trash failed!");
							e.printStackTrace();
						}
					}
					downloads.set(i, null);
				}
				else {
					found = index.lookup(file.getSha());
					if (found != null) {
						System.out.println("Downloading from local index: " + file);
						if (!simulate) {
							try {
								IOUtil.copy(new FileInputStream(found), new FileOutputStream(dest));
							}
							catch (IOException e) {
								System.out.println("Download from local index failed!");
								e.printStackTrace();
							}
						}
						downloads.set(i, null);
					}
				}
			}
		}
		System.out.println("Downloading files from remote locations.");
		// second pass, download files
		for (int i = 0; i < downloads.size(); i++) {
            progress.setProgress(100.0 * i / downloads.size());
			HashedFile file = downloads.get(i);
			if (file != null) {
				File dest = new File(basedir, file.getFile());
				// try to download
				URL url = new URL(download + "/" + file.getSha());
				System.out.println("Downloading from server: " + file + " <- " + url);
				if (!simulate) {
					try {
						IOUtil.copy(url.openStream(), new FileOutputStream(dest));
					}
					catch (IOException e) {
						System.out.println("Download from server failed!");
						e.printStackTrace();
					}

				}
			}
		}
		progress.setProgress(100);
		System.out.println("Done.");
		return so;
	}

	private void check(File basedir, File directory, SyncOps so, Config oldConfig) throws Exception {
		System.out.println("Checking folder: " + directory);
		File[] files = directory.listFiles();
		int index = basedir.getPath().length() + 1;
		for (File f : files) {
			if (f.isFile()) {
				String file = f.getPath().substring(index).replace('\\', '/');
				String sha = lookup(file);
				if (sha == null) {
					if (!ignore(file)) {
						so.delete(file, IOUtil.calculateSHA(new FileInputStream(f)));
					}
				}
				else {
					FileData data = oldConfig.get(file);
					if (data == null || !data.equals(f)) {
						data = new FileData(f);
						System.out.println("File has changed: " + file);
					}
					m_indexer.getConfig().put(file, data);

					String localsha = data.getHash();
					if (sha.equals(localsha)) {
						so.found(new HashedFile(file, sha));
					}
					else {
						System.out.println("Mismatch: " + file + " local " + localsha + " remote " + sha);
						so.delete(file, sha);
					}
				}
			}
		}
		for (File f : files) {
			if (f.isDirectory()) {
				check(basedir, f, so, oldConfig);
			}
		}
	}
}
