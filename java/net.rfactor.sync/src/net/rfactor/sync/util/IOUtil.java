package net.rfactor.sync.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * Helper class with IO utilities.
 */
public class IOUtil {
	public static String calculateSHA(InputStream stream) throws Exception {
		String sha1 = "";
		byte[] buffer = new byte[4096];
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			int b = stream.read(buffer);
			while (b != -1) {
				crypt.update(buffer, 0, b);
				b = stream.read(buffer);
			}
			sha1 = IOUtil.byteToHex(crypt.digest());
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		finally {
			stream.close();
		}
		return sha1;
	}

	public static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	public static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[4096];
		int length;
		try {
			while ((length = in.read(buffer)) != -1) {
				out.write(buffer, 0, length);
			}
		}
		finally {
			in.close();
			out.close();
		}
	}

	public static void move(File from, File to) throws IOException {
		if (!from.renameTo(to)) {
			throw new IOException("Could not rename from " + from + " to " + to);
		}
	}

	public static void delete(File from) throws IOException {
		if (!from.delete()) {
			throw new IOException("Could not delete " + from);
		}
	}
}
