package net.rfactor.sync;

import java.io.File;
import java.util.Set;

public interface Index {
	public File lookup(String hash);
	public Set<String> hashes();
}
