package net.rfactor.hud.standings;

import com.google.gson.annotations.SerializedName;

public class Vehicle {
    @SerializedName("driver") public String driverName;
    @SerializedName("position") public int position;
    @SerializedName("positionInClass") public int positionInClass;
    @SerializedName("vehicle") public String vehicleName;
    @SerializedName("class") public String vehicleClass; 
    @SerializedName("qualPosition") public int qualificationPosition;
    @SerializedName("distance") public float currentLapDistance;
    @SerializedName("blueFlags") public int blueFlags;
    
    @Override
    public String toString() {
        return "Vehicle[" + 
            driverName + " " + 
            vehicleName + " " + 
            vehicleClass + " " + 
            position + " " + 
            positionInClass + " " + 
            qualificationPosition + " " + 
            currentLapDistance + " " + 
            blueFlags + "]";
    }
}
