package net.rfactor.broadcast.controller;

public interface BroadcastService {
    /** The unique identity for the settings of this service. */
    public static final String SERVICE_PID = "broadcast";
    
    public void sendMessage(String message);
    
    public void setPause(boolean pause);
    
    public boolean getPause();
}
